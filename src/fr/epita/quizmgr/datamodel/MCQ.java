package fr.epita.quizmgr.datamodel;

import java.util.List;

/**
 * @author KadySwar
 *
 */
public class MCQ  extends Question{
	private List<MCA> answerList;

	public List<MCA> getAnswerList() {
		return answerList;
	}

	public void setAnswerList(List<MCA> answerList) {
		this.answerList = answerList;
	}
	
}
