package fr.epita.quizmgr.datamodel;

/**
 * @author KadySwar
 *
 */
public class MCA extends Answer{
	
	private boolean isCorrectOption;

	public boolean isCorrectOption() {
		return isCorrectOption;
	}

	public void setCorrectOption(boolean isCorrectOption) {
		this.isCorrectOption = isCorrectOption;
	}

}
